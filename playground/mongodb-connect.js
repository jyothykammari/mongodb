// const MongoClient = require('mongodb').MongoClient;

const {MongoClient, ObjectId} = require('mongodb');

// var obj = new ObjectId();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) =>{
if(err){
  return  console.log('unable to connect to MongoDb server.');
}
console.log('Connected to MongoDb server');
const db = client.db('TodoApp');

// db.collection('Todos').insertOne({
// text: 'Something to do',
// completed: false
// }, (err, result) =>{
// if(err){
//     return console.log('Unable to insert to Todo',  err);
// }
// console.log(JSON.stringify(result.ops, undefined, 2));
// })

db.collection('Users').insertOne({
   //_id: 123,
    name: 'Jyothy',
    age: 10,
    location: 'Boston'
}, (err, result) =>
{
    if(err)
    {
        return console.log('unable to insert to Users', err);
    }
    console.log(JSON.stringify(result.ops, undefined, 2));
    // console.log(result.ops[0]._id.getTimestamp())
})

var user = {name: 'Jyothy', location: 'Boston'};
var {name} = user;
console.log(name);

client.close();
} );

