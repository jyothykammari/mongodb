// const MongoClient = require('mongodb').MongoClient;

const {MongoClient, ObjectId} = require('mongodb');

// var obj = new ObjectId();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) =>{
if(err){
  return  console.log('unable to connect to MongoDb server.');
}
console.log('Connected to MongoDb server');
const db = client.db('TodoApp');

// db.collection('Todos').find({
//     _id:new ObjectId('5bbcbd2adef0a9ce30f18391')
// }).toArray().then((docs)=>{
// console.log('todos');
// console.log(JSON.stringify(docs, undefined, 2));
// }, (err) => {
//     console.log('Unable to fetch todos', err);
// })

// db.collection('Todos').find().count().then((count)=>{
// console.log(`todos: ${count}`);
// }, (err) => {
//     console.log('Unable to fetch todos', err);
// })

db.collection('Users').find({name:'Jyothy'}).toArray().then((docs) =>{
    console.log(JSON.stringify(docs, undefined,2));
})



client.close();
} );

